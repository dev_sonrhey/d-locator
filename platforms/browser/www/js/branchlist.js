$(document).on('click', '.tradechannel-list div', function(){
	// alert($(this).text());
	var tradechannel = getParameterByName("tradechannel");
	// alert(tradechannel);
	window.location.replace("dashboard.html?tradechannel="+tradechannel+"&branch="+this.id);
});

var loadbranch = $(function(){
	var ipconfig = new ipConfig();
	$.ajax({
		type: "get",
		beforeSend: function(){
			// $('#loadermodal').modal("show");
			$('#loader').show();
		},
		url: ipconfig.ip+"/branchlist.php",
		success: function(response){
			$('#loader').hide();
			// $('#loadermodal').modal("hide");
			$('.tradechannel-list').empty();
			$('.tradechannel-list').html(response);
		},
		error: function(err){
			console.log(err.responseText);
		}
	});
});
