var myLatLng = {};
var bounds, map, marker1, marker2, dist;




$(document).ready(function(){

function initMap(){
	bounds = new google.maps.LatLngBounds();
    var directionsDisplay = [];
    var directionsService = new google.maps.DirectionsService;

    map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
        zoom: 14
    });

}
	var ipconfig = new ipConfig();
	var ccode;
	var tradechannel = getParameterByName("tradechannel");
    var branch = getParameterByName("branch");

	document.addEventListener("deviceready", onDeviceReady, false);

    function onDeviceReady() {
    	$('#loader').show();
    	$('#loadermessage').html('Getting your current location, please wait...');
    	var onSuccess = function(position) {
    		$('[name="customer"]').attr('disabled', false);
    		$('[name="customer"]').focus();
    		$('#loader').hide();
    		$('#loadermessage').html('Processing, please wait...');
    		$('#info-type').show();
    		$('#yloclat').val(position.coords.latitude);
    		$('#yloclong').val(position.coords.longitude);
    		var slt = $('#sloclat').val();
    		var slng = $('#sloclong').val();

    		myLatLng = {
    			lat: Number(position.coords.latitude),
    			lng: Number(position.coords.longitude)
    		};

    		//initialize the map
    		initMap();

	    };

    // onError Callback receives a PositionError object
    //
    function onError(error) {
        console.log('code: '    + error.code    + '\n' +
              'message: ' + error.message + '\n');
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true });
        // console.log("navigator.geolocation works well");
    }

    $('input[name="customer"]').on('keyup', function(){
    	$('.customer-list').empty();
    	if(this.value == ""){
    		$('.customer-list').empty();
    		$('#loader').hide();
    		$('#info-type').show();
    		$('#info-error').hide();
    		$('#info-conn').hide();
    	}else if(this.value != ""){
    		$('.customer-list').empty();
    		$('#info-type').hide();
    		$('#info-error').hide();
    		$('#loader').fadeIn();
    		$('#info-conn').hide();
    		$.ajax({
    			type: "get",
    			url: ipconfig.ip+"/getcustomers.php",
    			data: {custname: this.value, tradechannel: tradechannel, branch: branch},
    			success: function(response){
    				$('#loader').hide();
    				if(response == 0){
	    				$('#info-error').show();
	    				$('.customer-list').empty();
    				}else{
    					$('#info-error').hide();
    					$('.customer-list').html(response);

    				}
    			},
    			error: function(err){
    				$('.customer-list').empty();
    				$('#loader').hide();
    				$('#info-conn').show();
    				console.log(err.responseText);
    			}
    		});
    	}
    	
    });

	$(document).on('click', '.customer-list li', function(){
		ccode = $(this).find('div#code').text();
		var cname = $(this).find('div#name .c_2').text();
		var long = $(this).find('#longitude').attr('data-longitude');
		var lat = $(this).find('#latitude').attr('data-latitude');
		$('#sloclat').val(lat);
		$('#sloclong').val(long);
		$('#ccode').val(ccode);
		$('#cname').val(cname);
		$('#customerview').modal("show");
	});
	$('#btnmatch').on('click', function(){
		var slocationlat = $('#sloclat').val();
		var slocationlong = $('#sloclong').val();

		var clocationlat = $('#yloclat').val();
		var clocationlong = $('#yloclong').val();
		
		if(slocationlat === clocationlat && slocationlong === clocationlong){
			$('#infomessage').modal("show");
			$('#infomessage .modal-header').removeClass("btn btn-danger").addClass("btn btn-success");
			$('#icon').css("color", "#4bbf73");
			$("#icon").html('<span class="fa fa-check fa-4x"></span>');
			$("#returnmessage h4 b").html("Location Matched!");
			$('#btnoverwrite').hide();
			locationmatched();
		}else{
			$('#infomessage').modal("show");
			$('#infomessage .modal-header').removeClass("btn btn-success").addClass("btn btn-danger");
			$('#icon').css("color", "#d9534f");
			$("#icon").html('<span class="fa fa-times fa-4x"></span>');
			$("#returnmessage h4 b").html("Location not Matched!");
			$('#btnoverwrite').show();
		}

	});

	$('#btnoverwrite').on("click", function(){
		var ipconfig = new ipConfig();
		var clocationlat = $('#yloclat').val();
		var clocationlong = $('#yloclong').val();
		var slt = $('#sloclat').val();
    	var slng = $('#sloclong').val();
    	var finaldist = 0;

    	if(slt != "" && slng != ""){
			dist = getDistanceFromLatLonInKm(slt,slng,clocationlat,clocationlong);
			finaldist = Number(dist).toFixed(2);
    	}
		

		if(confirm("Are you sure you want to overwrite the existing location to your current location?")){
			$('#infomessage').modal("hide");
			$('#loadermodal').modal("show");
			$.ajax({
				type: "post",
				url: ipconfig.ip+"/updatestoreloc.php",
				data: {ccode: ccode, newlat: clocationlat, newlong: clocationlong, finaldist:finaldist},
				success: function(response){
					$('#loadermodal').modal("hide");
					alert(response);
					window.location.reload();
				},
				error: function(err){
					console.log(err.responseText);
				}
			});
		}else{

		}
	});


	function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
	  var R = 6371; // Radius of the earth in km
	  var dLat = deg2rad(lat2-lat1);  // deg2rad below
	  var dLon = deg2rad(lon2-lon1); 
	  var a = 
	    Math.sin(dLat/2) * Math.sin(dLat/2) +
	    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
	    Math.sin(dLon/2) * Math.sin(dLon/2)
	    ; 
	  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	  var d = R * c; // Distance in km
	  return d;
	}

	function deg2rad(deg) {
	  return deg * (Math.PI/180)
	}


	function locationmatched(){
		$.ajax({
			type: "post",
			url: ipconfig.ip+"/storelocmatch.php",
			data: {ccode: ccode},
			success: function(response){
				alert(response);
				window.location.reload();
			},
			error: function(err){
				console.log(err.responseText);
			}
		});
	}

	$('#btnmap').on('click', function(){
		initMap();
        var clocationlat = $('#yloclat').val();
		var clocationlong = $('#yloclong').val();
		var slt = $('#sloclat').val();
    	var slng = $('#sloclong').val();

        //put marker for current loc with bouncing effect
		marker1 = new google.maps.Marker({
                        position: {
                            lat: Number(clocationlat),
                            lng: Number(clocationlong)
                        },
                        map: map,
                        icon: {
                        	url: "img/usericon.png",
		                    scaledSize: new google.maps.Size(55, 55),
		                    origin: new google.maps.Point(0, 0),
		                    anchor: new google.maps.Point(40, 50)
                        },
                        optimized: false,
                        animation: google.maps.Animation.BOUNCE,
                        zIndex: 100
                    });

                    bounds.extend(marker1.position);

        // //put marker for old store loc
        marker2 = new google.maps.Marker({
            position: {
                lat: Number(slt),
                lng: Number(slng)
            },
            map: map,
            optimized: false,
            zIndex: 100
        });

        bounds.extend(marker2.position);

        var clocationlat = $('#yloclat').val();
		var clocationlong = $('#yloclong').val();
		var slt = $('#sloclat').val();
    	var slng = $('#sloclong').val();
    	var finaldist = 0;

    	if(slt != "" && slng != ""){
			dist = getDistanceFromLatLonInKm(slt,slng,clocationlat,clocationlong);
			finaldist = Number(dist).toFixed(2);
			$('#dist').html(finaldist);
    	}

		$('#mapsmodal').modal("show");
	});

	$('#btnclose').on('click', function(){
		marker1.setMap(null);
		marker2.setMap(null);
		$('#mapsmodal').modal("hide");
	});
});