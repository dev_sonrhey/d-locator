var loadtradechannels = $(function(){
	// $('#loadermodal').modal("show");
	var ipconfig = new ipConfig();
	$.ajax({
		type: "get",
		beforeSend: function(){
			// $('#loadermodal').modal("show");
			$('#loader').show();
		},
		url: ipconfig.ip+"/tradechannellist.php",
		success: function(response){
			$('#loader').hide();
			// $('#loadermodal').modal("toggle");
			$('.tradechannel-list').empty();
			$('.tradechannel-list').html(response);
		},
		error: function(err){
			console.log(err.responseText);
		}
	});
});

$(document).on('click', '.tradechannel-list div', function(){
	// alert($(this).text());
	window.location.replace("branchlist.html?tradechannel="+this.id);
});